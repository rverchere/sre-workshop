# SRE Workshop

Simple docker-compose project to run an application with metrics, and play with prometheus/grafana

## Setup

The compose file starts 3 applications:

- Spring Petclinic Metrics, a java application with some metrics

- Prometheus, a monitoring application that scrapes the petclinic metrics
  The default configuration is in the `prometheus/` folder
  A volume is created to persist data in case of reboot

- Grafana, a dashboard application linked with Prometheus
  A volume is created to persist data in case of reboot

All applications expose ther port to be accessible from outside.

## Run

```
$ docker compose up -d
```

To access to the application : http://localhost:8080
To access to prometheus : http://localhost:9090
To access to grafana : http://localhost:3000

## Reload Prometheus

```
$ docker exec -it sre-workshop-prometheus-1 kill -SIGHUP 1
```

## Get logs

```
$ docker compose logs -f
```